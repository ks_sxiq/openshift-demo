# openshift-demo

This demo is suitable for demonstrating some of the Openshift features

## Prerequisites

Openshift Cluster Running on AWS as some of the demo features aws specific underlying AWS Components such as EBS, EC2 etc


#### Each demo has its own Readme, so browse through the folders to read the respective Readme.


## Authors
Kumaresh Sundaramurthy/SXiQ, an IBM Company

