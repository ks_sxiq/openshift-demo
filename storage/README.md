# Storage

The Container Storage Interface (CSI) allows OpenShift Container Platform to consume storage from storage back ends that implement the CSI interface as persistent storage.

## CSI Drivers

CSI drivers are typically shipped as container images. These containers are not aware of OpenShift Container Platform where they run. To use CSI-compatible storage back end in OpenShift Container Platform, the cluster administrator must deploy several components that serve as a bridge between OpenShift Container Platform and the storage driver.

## Dynamic provisioning of Persistent Volumes(PVs)

Dynamic provisioning of persistent storage depends on the capabilities of the CSI driver and underlying storage back end. The provider of the CSI driver should document how to create a storage class in OpenShift Container Platform and the parameters available for configuration.

In this example `pvc-ebs.yaml` , we have used EBS CSI driver which allows dynamic provisioning of the EBS Volumes and automatically create the Persistent Volume(PV) when the Persistent Volume Claim(PVC) is claimed by a pod.

Before you provision a PersistentVolume using the CSI Driver, an appropriate StorageClass relevant to the storage platform needs to be deployed. For example to provision EBS persistent Volume, a compatible EBI CSI Storage Class needed to be deployed as given in example `storageClass-EBS.yaml`.


The `csi-pod.yaml` example demonstrates attachment of newly created PVC using `pvc-ebs.yaml` mounted as a FileSystem in the Pod.


<em> execute the following to deploy the StorageClass </em>
```
ks@ubuntu:/# oc apply -f storageClass-ebs.yaml
```

<em> Run the following to dynamically provision PV </em>
```
ks@ubuntu:/# oc apply -f pv-ebs.yaml
persistentvolumeclaim/mypvc-ebs created
```

<em> Run the following to claim the PVC using a pod </em>
```
ks@ubuntu:/# oc apply -f csi-pod.yaml
pod/csi-ebs-pod created
```

## Volume Snapshots

A snapshot represents the state of the storage volume in a cluster at a particular point in time. Volume snapshots can be used to provision a new volume.

OpenShift Container Platform supports Container Storage Interface (CSI) volume snapshots by default. However, a specific CSI driver is required.

To perform a snapshot, an appropriate SnapshotClass needs to be configured, the snapshotclass should use the same driver that is used to create StorageClass of the underlying storage type. In this case an AWS EBS Volume.

<em> execute the following to deploy the VolumeSnapShotClass </em>
```
ks@ubuntu:/# oc apply -f volumeSnapshotClass.yaml
```

Now create the snapshot of the PVC that we created before using `pvc-ebs.yaml`

<em> execute the following to deploy the VolumeSnapShot </em>
```
ks@ubuntu:/# oc apply -f volumeSnapshot.yaml
```
It will take some time to create a snapshot based on its size, you can verify the status by running a get or describe on them
```
ks@ubuntu:/# oc get VolumeSnapshot -A
```

## Volume Restore

In this demo we will consume the snapshot created and verify its files. To do that we will create a PVC using the snapshot created above and then use a new pod to mount them to claim the PV. We will then verify the filesystem to be sure the snapshot contained everything that we created before.

<em> Run the following to create the PVC using the snapshot created before </em>
```
ks@ubuntu:/# oc apply -f volumeRestore-ebs.yaml
```

<em> We will now deploy a pod and mount the restored PVC as filesystem </em>
```
ks@ubuntu:/# oc apply -f csi-pod-restore.yaml
```

The next step is to login to the new pod created with the restored snapshot and verify its filesystem and contents.

## Scheduled Snapshots

The example `schedule-pvc-backup-job.yaml` shows how you can run snapshots in a schedule using an openshift CronJob. The CronJob creates a pod every 1 minute. The pod was built using an openshift4 cli image which uses Service Accounts with correct priveleges to operate on the VolumeSnapshots Custom Resource Definitions(CRDs).

## References


1. CSI Architecture: <https://docs.openshift.com/container-platform/4.11/storage/container_storage_interface/persistent-storage-csi.html>
2. CSI Volume Snapshots: <https://docs.openshift.com/container-platform/4.11/storage/container_storage_interface/persistent-storage-csi-snapshots.html>
3. CSI Interface: <https://github.com/container-storage-interface/spec>
4. Openshift Cron Jobs: <https://docs.openshift.com/container-platform/4.12/nodes/jobs/nodes-nodes-jobs.html>


## Authors
Kumaresh Sundaramurthy/SXiQ, an IBM Company

