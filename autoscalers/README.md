# autoscalers

## nginx-deployment.yaml

An example nginx deployment which has very restrictive resources(cpu and memory) to demonstrate Autoscalers.

## hpa-nginx.yaml

HPA stands for 'Horizontal Pod Autoscaler'. HPA can be used to specify how OpenShift Container Platform should automatically increase or decrease the scale of a replication controller or deployment configuration, based on metrics collected from the pods that belong to that replication controller or deployment configuration. 

You can create an HPA for any Deployment, DeploymentConfig, ReplicaSet, ReplicationController, or StatefulSet object.

The example `hpa-nginx.yaml` ensures a minimum of 3 replicas are available for nginx-deployment although the deployment file suggests 2 replica-set and hpa will auto-scale based on the configuration, in this case CPU and it will scale up to 7 pods horizontally. 


## vpa-nginx.yaml

VPA stands for 'Vertical Pod Autoscaler' unlike HPAs they dont come shipped with Openshift by default, VPAs needs to be installed via the Operator hub.

The OpenShift Container Platform VPA automatically reviews the historic and current CPU and memory resources for containers in pods and can update the resource limits and requests based on the usage values it learns. The VPA uses individual custom resources (CR) to update all of the pods associated with a workload object, such as a Deployment, DeploymentConfig, StatefulSet, Job, DaemonSet, ReplicaSet, or ReplicationController, in a project.

The VPA helps you to understand the optimal CPU and memory usage for your pods and can automatically maintain pod resources through the pod lifecycle.

In this example `vpa-nginx.yaml` VPAs automatically increase the cpu size based on the current and historical utilization patterns. 

## References


1. Machine Management: <https://docs.openshift.com/container-platform/4.11/machine_management/index.html>
2. HPA: <https://docs.openshift.com/container-platform/4.11/nodes/pods/nodes-pods-autoscaling.html>
3. VPA: <https://docs.openshift.com/container-platform/4.11/nodes/pods/nodes-pods-vertical-autoscaler.html>


## Authors
Kumaresh Sundaramurthy/SXiQ, an IBM Company

