# Compute

## machine-set.yaml

MachineSet resources are groups of machines. Machine sets are to machines as replica sets are to pods. If you need more machines or must scale them down, you change the replicas field on the machine set to meet your compute need.

Machine-sets can be defined for any Cloud or Platform that OpenShift supports.

In this example `machine-set.yaml` we have demonstrated how to create a machine-set for AWS.

## node-affinity.yaml
Node affinity allows a pod to specify an affinity towards a group of nodes it can be placed on. The node does not have control over the placement.

For example, you could configure a pod to only run on a node with a specific CPU or in a specific availability zone.

There are two types of node affinity rules: required and preferred.

Required rules must be met before a pod can be scheduled on a node. Preferred rules specify that, if the rule is met, the scheduler tries to enforce the rules, but does not guarantee enforcement.

This example `node-affinity.yaml` uses a preferred rule to look for an instance type matching m3.large, if the preferred node is not available it will pick whatever is available.  

## node-selector.yaml
A node selector specifies a map of key-value pairs. The rules are defined using custom labels on nodes and selectors specified in pods.

For the pod to be eligible to run on a node, the pod must have the indicated key-value pairs as the label on the node.

This example `node-selector.yaml` uses a node-selector rule to look for an instance type matching t3.xlarge, if the instance type or the label is not available then the pods wont get scheduled at all.


## References


1. Machine Management: <https://docs.openshift.com/container-platform/4.11/machine_management/index.html>
2. Node Affinity: <https://docs.openshift.com/container-platform/4.11/nodes/scheduling/nodes-scheduler-node-affinity.html>
3. Node Selectors: <https://docs.openshift.com/container-platform/4.11/nodes/pods/nodes-pods-node-selectors.html>


## Authors
Kumaresh Sundaramurthy/SXiQ, an IBM Company

